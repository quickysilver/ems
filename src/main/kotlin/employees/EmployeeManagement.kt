package employees

import java.time.LocalDate

fun main () {
//    val employee2 = AdminEmployee(
//        "Brandon",
//        "Cruz",
//        "HR",
//        LocalDate.now()
//    )

    //with, apply, let, run

//    with(employee2) {
//        println(firstName)
//        println(lastName)
//        println(id)
//        println(department)
//        println(isRankAndFile)
//        println(isAdmin)
//    }
//    println(employee2.firstName)
//    employee2.apply {
//        this.firstName = firstName.uppercase()
//    }
    val address1 = LocationDetails(
        "Makati",
        "Metro Manila",
        "NCR",
        "1124 XYZ Building, Legaspi Village"
    )
    val employee1 = RankAndFileEmployee(
        "Shelly",
        "Ancheta",
        "IT",
        LocalDate.now(),
        address1
    )

    val address2 = address1.copy(
        exactAddress = "1623 HJK Building, Salcedo Village"
    )
    val employee2 = RankAndFileEmployee(
        "Shelly",
        "Ancheta",
        "IT",
        LocalDate.now(),
        address2
    )
    println(employee1 == employee2)
    println(address1)
    println(address2)
}
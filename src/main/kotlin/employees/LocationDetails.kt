package employees

data class LocationDetails (
    var city: String,
    var province: String,
    var Region: String,
    var exactAddress: String
)
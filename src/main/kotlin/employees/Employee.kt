package employees

import java.time.LocalDate

abstract class Employee (
    override var firstName: String,
    override var lastName: String,
    var department: String,
    val hiringDate: LocalDate,
    var location: LocationDetails
) : EmployeeInterface {
    var id: String = ""

    override fun doSomething(): String = "Sample String"

    /*
    Create a function that shows the data of this employee
    * "Employee Name:  from  department."
    */

    fun employeeData (): String{
        return "Employee Name: $firstName $lastName from $department department."
    }

    /*
    * When this class is instantiated, generate an id, and save as an attribute of this class
    * the id format is: first 2 letters of the first name + - + first 2 letters off
    * the lastName + - + 8 random digits
    */

    init {
        this.id = "${firstName.substring(0, 2).uppercase()}-${
            lastName.substring(0, 2).uppercase()
        }-${(10000000 until 99999999).random()}"
    }
}

class Person(
    override var firstName: String,
    override var lastName: String,
): EmployeeInterface {
    override fun doSomething(): String {
        TODO("Not yet implemented")
    }
}
package employees

import java.time.LocalDate

class AdminEmployee(
    firstName: String,
    lastName: String,
    department: String,
    hiringDate: LocalDate,
    location: LocationDetails,
//    isRankAndFileEmployee: Boolean = true,
//    isAdmin: Boolean = false
): Employee(
    firstName,
    lastName,
    department,
    hiringDate,
    location
){
    val isRankAndFile = false
    val isAdmin= true

    override fun doSomething(): String = isAdmin.toString()
}